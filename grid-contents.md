# Website Grid
## What Is The Website Grid?​
The website grid is a type of layout used to organize content on a website. It divides the page into columns and rows, allowing you to place content in specific areas.

![What is Gird?](https://www.freecodecamp.org/news/content/images/size/w2000/2022/05/CSS-GRID-3.png)


## When Should You Use CSS Grid ??
- When we have a complex design layout to implement.
- When we need a space or gap between elements.

## Types Of CSS Grid
![What is Gird?](https://elementor.com/cdn-cgi/image/f=auto,w=1024/marketing/wp-content/uploads/sites/9/2020/09/modular-grid-examples-1.png)
### 1. Block Grid 
- A Classic Option for Single Posts and Articles 
  
  https://elementor.com/cdn-cgi/image/f=auto,w=1244/marketing/wp-content/uploads/sites/9/2020/09/block-grid-elementor-example-1244x1536.png
  

### 2. Column Grid
- Column grids are composed of several columns, mostly used to organize multiple elements into columns. Column grids can have as little as two columns, with no real limit to how many there can be.
![](https://elementor.com/cdn-cgi/image/f=auto,w=1536/marketing/wp-content/uploads/sites/9/2020/09/column-grid-example-1-1536x1279.png)

### 3. Modular Grid
- Modular grids are composed of columns as well as rows. They’re often compared to or described as looking like a checkerboard, and can be very effective for presenting many things at once for easy access.
- Common use-cases for modular grids are mobile phone home screens that show the full collection of apps, or e-commerce websites that display collections of inventory in their category pages. 


https://elementor.com/blog/wp-content/uploads/sites/9/2020/09/luxury-real-estate-website-grids-post.mp4


### 4. Hierarchical Grid
- Hierarchical grids, which can also be described as ”freestyle”, are grids whose elements are placed “spontaneously” among the grid’s columns and rows. This means that the column widths and row heights vary throughout the grid. 
- The best example is fashion / photographer’s portfolio site - https://unsplash.com/